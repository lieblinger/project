# Copyright 2017 - 2018 Mflectralar <info@mflectralar.com>
# License LGPLv3.0 or later (https://www.gnu.org/licenses/lgpl-3.0.en.html).

{
    "name": "Project key",
    "summary": "Module decorates projects and tasks with ``key`` field",
    "category": "Project",
    "version": "1.0.1.0.0",
    "license": "LGPL-3",
    "author": "Mflectralar, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/project",
    "depends": [
        "project",
    ],
    "data": [
        "views/project_key_views.xml",
    ],
    "post_init_hook": "post_init_hook",
}
