{
    'name': 'Project Risk',
    'summary': 'MOR risk management method',
    'author': 'Onestein, Odoo Community Association (OCA)',
    'license': 'AGPL-3',
    'website': 'https://gitlab.com/flectra-community/project',
    'category': 'Project Management',
    'version': '1.0.1.0.0',
    'depends': [
        'project'
    ],
    'data': [
        'security/ir_model_access.xml',

        'data/project_risk_response_category_data.xml',
        'data/project_risk_category_data.xml',

        'views/project_risk_response_category_view.xml',
        'views/project_risk_category_view.xml',
        'views/project_risk_view.xml',
        'views/project_project_view.xml',

        'menuitems.xml',
    ],
    'installable': True,
}
