# Flectra Community / project

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[project_task_digitized_signature](project_task_digitized_signature/) | 1.0.1.0.0| Project Task Digitized Signature
[project_timesheet_time_control](project_timesheet_time_control/) | 1.0.1.2.0| Project timesheet time control
[project_wbs](project_wbs/) | 1.0.1.0.0| Project Work Breakdown Structure
[project_task_type_active](project_task_type_active/) | 1.0.1.0.0| Adds active field on project task type
[project_risk](project_risk/) | 1.0.1.0.0| MOR risk management method
[project_task_pull_request](project_task_pull_request/) | 1.0.1.0.0| Adds a field for a PR URI to project tasks
[project_task_dependency](project_task_dependency/) | 1.0.1.0.2| Enables to define dependencies (other tasks) of a task
[project_task_code](project_task_code/) | 1.0.1.1.0| Sequential Code for Tasks
[project_category](project_category/) | 1.0.1.1.0| Project Types
[project_task_material](project_task_material/) | 1.0.1.0.0| Record products spent in a Task
[project_task_send_by_mail](project_task_send_by_mail/) | 1.0.1.0.0| Send task report by email
[project_stage_closed](project_stage_closed/) | 1.0.1.0.0| Make the Closed flag on Task Stages available without installing sale_service
[project_description](project_description/) | 1.0.1.0.0| Add a description to projects
[project_timeline_hr_timesheet](project_timeline_hr_timesheet/) | 1.0.1.0.0| Shows the progress of tasks on the timeline view.
[project_task_add_very_high](project_task_add_very_high/) | 1.0.1.0.0| Adds extra options 'High' and 'Very High' on tasks
[project_key](project_key/) | 1.0.1.0.0| Module decorates projects and tasks with ``key`` field
[project_department](project_department/) | 1.0.1.0.0| Project Department Categorization
[project_stage_state](project_stage_state/) | 1.0.1.0.0| Restore State attribute removed from Project Stages in 8.0
[project_timeline_task_dependency](project_timeline_task_dependency/) | 1.0.1.0.0| Render arrows between dependencies.
[project_task_material_with_sale_timesheet](project_task_material_with_sale_timesheet/) | 1.0.1.0.0| Add compatibility between  project_task_material_stockand sale_timesheet module 
[project_hr](project_hr/) | 1.0.1.0.0| Link HR with project
[project_task_default_stage](project_task_default_stage/) | 1.0.1.0.0| Recovery default task stage projects from v8
[project_timeline_critical_path](project_timeline_critical_path/) | 1.0.1.0.0| Highlight the critical paths of your projects.
[project_task_material_stock](project_task_material_stock/) | 1.0.1.0.2| Create stock and analytic moves from record products spent in a Task
[project_timeline](project_timeline/) | 1.0.1.1.0| Timeline view for projects


