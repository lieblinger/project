# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0).
{
    'name': 'Project Header',
    'version': '1.0.1.0.0',
    'category': 'Project Management',
    'summary': 'Adds empty header on project forms.',
    'author': 'Flectra Community',
    'website': 'https://flectra-community.org',
    'license': 'LGPL-3',
    'depends': [
        'project'
    ],
    "data": [
        "views/project.xml",
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
