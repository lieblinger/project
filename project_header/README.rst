==================================
Project Header
==================================

Adds empty header on project forms.

Usage
=====

* After module installation an empty header on project forms will be displayed.

Authors
~~~~~~~

Yannik Lieblinger


Maintainers
~~~~~~~~~~~

This module is maintained by https://flectra-community.org
