from flectra import fields, models


class ProjectProject(models.Model):
    _inherit = 'project.project'

    type_id = fields.Many2one(
            comodel_name='project.type',
            string='Type',
            copy=False,
            domain="[('project_ok', '=', True)]",
    )

    default_task_type_id = fields.Many2one(
            comodel_name='project.type',
            string='Default Task Type',
            domain="[('task_ok', '=', True)]",
    )
