from flectra import fields, models, api


class ProjectTask(models.Model):
    _inherit = 'project.task'

    @api.model
    def _default_type_id(self):
        project_id = self.env.context.get('default_project_id')
        if project_id:
            project = self.env['project.project'].browse(project_id)
            return project.default_task_type_id

    type_id = fields.Many2one(
            comodel_name='project.type',
            string='Type',
            domain="[('task_ok', '=', True)]",
            default=_default_type_id,
    )

    @api.onchange('project_id')
    def _onchange_project(self):
        result = super()._onchange_project()
        if self.project_id.default_task_type_id:
            self.type_id = self.project_id.default_task_type_id
        return result

    def action_subtask(self):
        action = super().action_subtask()
        action['context']['default_type_id'] = self.env.context.get('type_id', self.type_id.id)
        return action
