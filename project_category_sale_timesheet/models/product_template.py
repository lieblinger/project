import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    project_type_id = fields.Many2one(
            comodel_name="project.type",
            string="Task Type",
            domain="[('task_ok', '=', True)]",
    )
