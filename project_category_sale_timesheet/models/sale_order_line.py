import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _timesheet_create_task_prepare_values(self):
        result = super()._timesheet_create_task_prepare_values()
        result['type_id'] = self.product_id.project_type_id.id
        return result
