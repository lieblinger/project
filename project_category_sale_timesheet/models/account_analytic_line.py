import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    type_id = fields.Many2one(
            comodel_name='project.type',
            string='Type',
    )

    @api.model
    def _timesheet_preprocess(self, values):
        values = super(AccountAnalyticLine, self)._timesheet_preprocess(values)

        if 'project_id' in values and 'task_id' not in values:
            project = self.env['project.project'].sudo().browse(values['project_id'])
            if project.type_id:
                values['type_id'] = project.type_id.id or values.get('type_id', False)

        if 'task_id' in values:
            task = self.env['project.task'].sudo().browse(values['task_id'])
            if task.type_id:
                values['type_id'] = task.type_id.id or task.project_id.type_id.id or values.get('type_id', False)

        return values
