{
    'name': 'Project Types with Sale Timesheet',
    'version': '1.0.1.1.0',
    'category': 'Project',
    'author': 'Flectra Community',
    'website': 'https://gitlab.com/flectra-community/project',
    'license': 'AGPL-3',
    'depends': [
        'project_category',
        'sale_timesheet',
    ],
    'data': [
        'views/product_views.xml',
        'views/account_analytic_views.xml',
    ],
    'installable': True,
}
